package com.boost.watchcore.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.boost.template.R;
import com.boost.watchcore.model.PushTokenModel;
import com.boost.watchcore.model.User;
import com.boost.watchcore.ui.global.AppMain;
import com.boost.watchcore.utils.Constant;
import com.boost.watchcore.utils.PreferencesManager;
import com.boost.watchcore.utils.Utils;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;

import java.io.IOException;

/**
 * Created by oliver on 21/01/16.
 */
public class AuthenticationActivity extends BaseActivity implements View.OnClickListener{

    private final String LOG_TAG = AuthenticationActivity.this.getClass().getSimpleName();

    public static final int RC_GOOGLE_LOGIN = 1;

    private ProgressDialog mAuthProgressDialog;
    private Firebase mFirebaseRef;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mSharedPrefEditor;
    private Firebase.AuthStateListener mAuthStateListener;

    private boolean mGoogleIntentInProgress;
    private String mEncodedEmail;
    GoogleSignInAccount mGoogleAccount;
    private Button mGoogleSignIn;
    private TextView mAnonymousSignIn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppMain)getApplication()).trackScreen(
                this.getClass().getSimpleName(),
                null);
        setContentView(R.layout.activity_authentication);
        initUi();
        setupProgressDialog();
        mFirebaseRef = new Firebase(Constant.FIREBASE_URL);
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        mSharedPrefEditor = mSharedPref.edit();
        findViewById(R.id.ivBanner_authentication_activity).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });
    }

    private void initUi() {
        mGoogleSignIn = (Button) findViewById(R.id.btn_sign_in_with_google);
        mAnonymousSignIn = (TextView) findViewById(R.id.btn_sign_in_anonymous);
        mGoogleSignIn.setOnClickListener(this);
        mAnonymousSignIn.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * This is the authentication listener that maintains the current user session
         * and signs in automatically on application launch
         */
        mAuthStateListener = new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                mAuthProgressDialog.dismiss();

                /**
                 * If there is a valid session to be restored, start MainActivity.
                 * No need to pass data via SharedPreferences because app
                 * already holds userName/provider data from the latest session
                 */
                if (authData != null) {
                    startMainActivity();
                }
            }
        };
        mFirebaseRef.addAuthStateListener(mAuthStateListener);
    }

    /**
     * Cleans up listeners tied to the user's authentication state
     */
    @Override
    public void onPause() {
        super.onPause();
        mFirebaseRef.removeAuthStateListener(mAuthStateListener);
    }

    private void setupProgressDialog() {
        // Setup the progress dialog that is displayed later when authenticating with Firebase
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle(getResources().getString(R.string.progress_dialog_loading));
        mAuthProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_wait_for_authenticating));
        mAuthProgressDialog.setCancelable(false);
    }

    public void startMainActivity() {
        //Go to main activity
        Intent intent = new Intent(AuthenticationActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_in_with_google:
                googleSignInClick();
                break;
            case R.id.btn_sign_in_anonymous:
                anonymousSignIn();
                break;
        }
    }

    private void anonymousSignIn() {
        mAuthProgressDialog.show();
        mFirebaseRef.authAnonymously(new MyAuthResultHandler(Constant.ANONYMOUS_PROVIDER));
    }

    // =================== Google Section =====================

    /**
     * Handle user authentication that was initiated with mFirebaseRef.authWithPassword
     * or mFirebaseRef.authWithOAuthToken
     */
    private class MyAuthResultHandler implements Firebase.AuthResultHandler {

        private final String provider;

        public MyAuthResultHandler(String provider) {
            this.provider = provider;
        }

        /**
         * On successful authentication call setAuthenticatedUser if it was not already
         * called in
         */
        @Override
        public void onAuthenticated(AuthData authData) {
            mAuthProgressDialog.dismiss();
            Log.i(LOG_TAG, provider + " " + getString(R.string.log_message_auth_successful));
            ((AppMain) getApplication()).
                    trackEvent(getString(R.string.analytics_category_authentication),
                            getString(R.string.analytics_action_auth_success),
                            provider);
            if (authData != null) {
                /**
                 * If user has logged in with Google provider
                 */
                if (authData.getProvider().equals(Constant.GOOGLE_PROVIDER)) {
                    setAuthenticatedUserGoogle(authData);
                    SendTokenToPushNotifications(PreferencesManager.getToken(AuthenticationActivity.this), mEncodedEmail);
                } if (authData.getProvider().equals(Constant.ANONYMOUS_PROVIDER)) {
                    SendTokenToPushNotifications(PreferencesManager.getToken(AuthenticationActivity.this), "");
                }  else {
                    Log.e(LOG_TAG, getString(R.string.log_error_invalid_provider) + authData.getProvider());
                }

                PreferencesManager.setProvider(AuthenticationActivity.this, authData.getProvider());

                startMainActivity();
            }
        }

        /**
         * Push users id and deviceToken to Firebase
         */
        private void SendTokenToPushNotifications(String token, String mail) {
            final Firebase pushNotificationRef = new Firebase(Constant.FIREBASE_URL_PUSHNOTIFICATIONS);
            PushTokenModel pushToken = new PushTokenModel(token, mail);
            pushNotificationRef.push().setValue(pushToken);
            PreferencesManager.writtenTokenToDb(AuthenticationActivity.this, true);
        }

        @Override
        public void onAuthenticationError(FirebaseError firebaseError) {
            mAuthProgressDialog.dismiss();

            /**
             * Use utility method to check the network connection state
             * Show "No network connection" if there is no connection
             * Show Firebase specific error message otherwise
             */
            if (firebaseError.getCode() == FirebaseError.NETWORK_ERROR)
                    showErrorToast(getString(R.string.error_message_failed_sign_in_no_network));
        }
    }

    /**
     * Signs you into firebase using the Google Login Provider
     *
     * @param token A Google OAuth access token returned from Google
     */
    private void loginWithGoogle(String token) {
        mFirebaseRef.authWithOAuthToken(Constant.GOOGLE_PROVIDER, token, new MyAuthResultHandler(Constant.GOOGLE_PROVIDER));
    }

    /**
     * Helper method that makes sure a user is created if the user
     * logs in with Firebase's Google login provider.
     *
     * @param authData AuthData object returned from onAuthenticated
     */
    private void setAuthenticatedUserGoogle(AuthData authData) {
        /**
         * If google api client is connected, get the lowerCase user email
         * and save in sharedPreferences
         */
        String unprocessedEmail;
        if (mLoginGoogleApiClient.isConnected()) {
            unprocessedEmail = mGoogleAccount.getEmail().toLowerCase();
            PreferencesManager.setUserEmailGoogle(AuthenticationActivity.this, unprocessedEmail);
        } else {

            /**
             * Otherwise get email from sharedPreferences, use null as default value
             * (this mean that user resumes his session)
             */
            unprocessedEmail = PreferencesManager.getUserEmailGoogle(AuthenticationActivity.this);
        }

        /**
         * Encode user email replacing "." with "," to be able to use it
         * as a Firebase db key
         */
        mEncodedEmail = Utils.encodeEmail(unprocessedEmail);

        /* Get username from authData */
        final String userName = (String) authData.getProviderData().get(Constant.PROVIDER_DATA_DISPLAY_NAME);

        /* If no user exists, make a user */
        final Firebase userLocation = new Firebase(Constant.FIREBASE_URL_USERS).child(mEncodedEmail);
        userLocation.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    User newUser = new User(userName, mEncodedEmail);

                    long time = System.currentTimeMillis();
                    newUser.setAccountCreationTime(String.valueOf(time));
                    newUser.setLastVisitTime(String.valueOf(time));
                    userLocation.setValue(newUser);
                } else {
                    userLocation.child(Constant.FIREBASE_PROPERTY_LAST_VISIT_TIME).
                            setValue(System.currentTimeMillis());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.d(LOG_TAG, getString(R.string.log_error_occurred) + firebaseError.getMessage());
            }
        });
    }

    private void googleSignInClick() {
        Log.d(LOG_TAG, "GooglePlus onClick isConnected: " + mLoginGoogleApiClient.isConnected());
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mLoginGoogleApiClient);
        startActivityForResult(signInIntent, RC_GOOGLE_LOGIN);
        mAuthProgressDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(LOG_TAG, "onActivityResult requestCode: " + requestCode + " resultCode: " + resultCode +
                " intent: " + data);
        if (requestCode == RC_GOOGLE_LOGIN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(LOG_TAG, "handleSignInResult:" + result.getStatus().toString());
        if (result.isSuccess()) {
            /* Signed in successfully, get the OAuth token */
            mGoogleAccount = result.getSignInAccount();
            getGoogleOAuthTokenAndLogin();
        } else {
            mAuthProgressDialog.dismiss();
        }
    }

    /**
     * Gets the GoogleAuthToken and logs in.
     */
    private void getGoogleOAuthTokenAndLogin() {
        /* Get OAuth token in Background */
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            String mErrorMessage = null;

            @Override
            protected String doInBackground(Void... params) {
                String token = null;

                try {
                    String scope = String.format(getString(R.string.oauth2_format), new Scope(Scopes.PROFILE)) + " email";

                    token = GoogleAuthUtil.getToken(AuthenticationActivity.this, mGoogleAccount.getEmail(), scope);
                } catch (IOException transientEx) {
                    /* Network or server error */
                    Log.e(LOG_TAG, getString(R.string.google_error_auth_with_google) + transientEx);
                    mErrorMessage = getString(R.string.google_error_network_error) + transientEx.getMessage();
                } catch (UserRecoverableAuthException e) {
                    Log.w(LOG_TAG, getString(R.string.google_error_recoverable_oauth_error) + e.toString());

                    /* We probably need to ask for permissions, so start the intent if there is none pending */
                    if (!mGoogleIntentInProgress) {
                        mGoogleIntentInProgress = true;
                        Intent recover = e.getIntent();
                        startActivityForResult(recover, RC_GOOGLE_LOGIN);
                    }
                } catch (GoogleAuthException authEx) {
                    /* The call is not ever expected to succeed assuming you have already verified that
                     * Google Play services is installed. */
                    Log.e(LOG_TAG, " " + authEx.getMessage(), authEx);
                    mErrorMessage = getString(R.string.google_error_auth_with_google) + authEx.getMessage();
                }
                return token;
            }

            @Override
            protected void onPostExecute(String token) {
                mAuthProgressDialog.dismiss();
                if (token != null) {
                    /* Successfully got OAuth token, now login with Google */
                    loginWithGoogle(token);
                } else if (mErrorMessage != null) {
                    showErrorToast(mErrorMessage);
                }
            }
        };

        task.execute();
    }

    // ============= Helpers methods =============

    private void showErrorToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
