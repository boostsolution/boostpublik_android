package com.boost.scooter.ui;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;

import com.boost.template.R;
import com.boost.template.WatchFaceUtil;
import com.boost.template.constants.TimeConstants;
import com.boost.template.ui.faces.BaseWatchFace;
import com.google.android.gms.wearable.DataMap;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by BruSD on 04.05.2015.
 */
public class ScooterWatchFace extends BaseWatchFace {
    private static final String TAG = "ScooterWatchFace";

    @Override
    public Engine onCreateEngine() {
        return new Engine();
    }

    private class Engine extends EngineBaseWatchFace {
        private final float HOUR_SIZE = 60f;
        private final float MINUTE_SIZE = 45f;
        private final float DAY_SIZE = 30f;
        private final float MONTH_SIZE = 30f;
        private final float BATTERY_SIZE = 28f;

        private final float HOUR_OFFSET_X = 145f;
        private final float HOUR_OFFSET_Y = 130f;

        private final float MINUTE_OFFSET_X = 160f;
        private final float MINUTE_OFFSET_Y = 125f;

        private final float DAY_OFFSET_X = 160f;
        private final float DAY_OFFSET_Y = 90f;

        private final float MONTH_OFFSET_X = 152f;
        private final float MONTH_OFFSET_Y = 120f;

        private final float BATTERY_OFFSET_X = 243f;
        private final float BATTERY_OFFSET_Y = 190f;

        private final float HOUR_ANGLE = -40f;
        private final float DATE_ANGLE = 42f;

        private TypedArray mBgTypedArray;
        private Typeface mTypeface;
        private Paint mDatePaint;
        private Paint mBatteryPaint;
        private SimpleDateFormat mHourFormat, mMinuteFormat;
        private SimpleDateFormat mMonthFormat, mDayFormat;

        private boolean mConfig_is12HourFormat  = true;
        private boolean mConfig_isLeadingZero = true;
        private boolean mConfig_isAmbientWithScooter = false;


        @Override
        protected void initVars() {
            PATH_WITH_FEATURE = getString(R.string.PATH_WITH_FUTURE);
            mGoogleApiClient.connect();

            mBgTypedArray = mResources.obtainTypedArray(R.array.scooter_img_set);
            mTypeface = Typeface.createFromAsset(getAssets(), "FIBYNGEROWA.OTF");

            mDatePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mDatePaint.setTypeface(mTypeface);

            mBatteryPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mBatteryPaint.setColor(mResources.getColor(R.color.scooter_battery));
            mBatteryPaint.setTypeface(mTypeface);

            setDateFormat(mConfig_isLeadingZero);
            setTimeFormat(mConfig_is12HourFormat, mConfig_isLeadingZero);

        }

        @Override
        protected void drawFace(Canvas canvas) {
            super.drawFace(canvas);
            drawTime(canvas);
            drawBattery(canvas);
            drawSecScooter(canvas);
        }

        @Override
        protected void drawAmbient(Canvas canvas) {
            super.drawAmbient(canvas);
            drawTime(canvas);
            drawBattery(canvas);
        }

        @Override
        protected void drawBg(Canvas canvas) {
            setBg();
            mMatrix.reset();
            mMatrix.postScale(mScaleValue, mScaleValue);
            canvas.drawBitmap(mBackgroundBitmap, mMatrix, mBitmapPaint);
        }

        @Override
        protected void setBg() {
            if (mBackgroundBitmap == null) {
                mBackgroundBitmap = getBackgroundBitmap();
            }
            mScaleValue = 2.0f * mCenterX / mBackgroundBitmap.getWidth();
        }

        private Bitmap getBackgroundBitmap() {
            if (isInAmbientMode()) {
                return BitmapFactory.decodeResource(mResources,
                        mConfig_isAmbientWithScooter ?
                        R.drawable.bg_ambient_scooter : R.drawable.bg_ambient);
            } else {
                return BitmapFactory.decodeResource(mResources, R.drawable.bg_scooter);
            }
        }

        private void drawSecScooter(Canvas canvas) {
            float angleSec = (6.0f * mTime.get(Calendar.SECOND) + 0.006f * mTime.get(Calendar.MILLISECOND));
            mMatrix.reset();
            mMatrix.postScale(mScaleValue, mScaleValue);
            mMatrix.postRotate(angleSec, mCenterX, mCenterY);
            canvas.drawBitmap(BitmapFactory.decodeResource(getResources(),
                            mBgTypedArray.getResourceId(mTime.get(Calendar.MILLISECOND) / (int) mConfig_UpdateTime, -1)),
                    mMatrix,
                    mBitmapPaint);
        }

        private void drawTime(Canvas canvas) {
            mDatePaint.setColor(isInAmbientMode() ?
                    Color.WHITE :
                    mResources.getColor(R.color.scooter_date));
            // draw Hours
            mDatePaint.setTextSize(HOUR_SIZE * mScaleValue);
            String hour = mHourFormat.format(mTime.getTime());
            canvas.save();
            canvas.rotate(HOUR_ANGLE, mCenterX, mCenterY);
            float textSize = mDatePaint.measureText(hour);
            canvas.drawText(hour,
                    HOUR_OFFSET_X * mScaleValue - textSize / 2,
                    HOUR_OFFSET_Y * mScaleValue,
                    mDatePaint);
            canvas.restore();

            // draw Minute
            mDatePaint.setTextSize(MINUTE_SIZE * mScaleValue);
            String min = mMinuteFormat.format(mTime.getTime());
            float textWidth = mDatePaint.measureText(min);
            canvas.drawText(min, MINUTE_OFFSET_X * mScaleValue - textWidth / 2,
                    MINUTE_OFFSET_Y * mScaleValue,
                    mDatePaint);

            //draw date
            canvas.save();
            canvas.rotate(DATE_ANGLE, mCenterX, mCenterY);
            mDatePaint.setTextSize(DAY_SIZE * mScaleValue);
            canvas.drawText(mDayFormat.format(mTime.getTime()),
                    DAY_OFFSET_X * mScaleValue,
                    DAY_OFFSET_Y * mScaleValue,
                    mDatePaint);

            if (!isInAmbientMode()) {
                mDatePaint.setColor(mResources.getColor(R.color.scooter_month));
            }
            canvas.drawText(mMonthFormat.format(mTime.getTime()),
                    MONTH_OFFSET_X * mScaleValue,
                    MONTH_OFFSET_Y * mScaleValue,
                    mDatePaint);
            canvas.restore();
        }

        private void drawBattery(Canvas canvas) {
            if (isInAmbientMode()) {
                mBatteryPaint.setColor(Color.WHITE);
            } else {
                mBatteryPaint.setColor(mResources.getColor(R.color.scooter_battery));
            }
            mBatteryPaint.setTextSize(BATTERY_SIZE * mScaleValue);
            canvas.drawText(getBatteryInfoWatch(),
                    BATTERY_OFFSET_X * mScaleValue,
                    BATTERY_OFFSET_Y * mScaleValue,
                    mBatteryPaint);
        }

        // ======================= SETTINGS AREA ================================
        protected void updateConfigDataItemAndUiOnStartup() {
            WatchFaceUtil.fetchConfigDataMap(mGoogleApiClient,
                    getString(R.string.PATH_WITH_FUTURE),
                    new WatchFaceUtil.FetchConfigDataMapCallback() {
                        @Override
                        public void onConfigDataMapFetched(DataMap startupConfig) {
                            WatchFaceUtil.putConfigDataItem(mGoogleApiClient,
                                    getString(R.string.PATH_WITH_FUTURE),
                                    startupConfig,
                                    null);

                            updateUiForConfigDataMap(startupConfig);
                        }
                    }
            );
        }

        protected void updateUiForConfigDataMap(final DataMap config) {
            Log.d(TAG, "updateUiForConfigDataMap config: " + config);
            for (String configKey : config.keySet()) {
                if (!config.containsKey(configKey)) {
                    continue;
                }
                boolean needInvalidate = false;

                if (WatchConstants.KEY_HOURS_FORMAT.equals(configKey)) {
                    needInvalidate = updateUiForKey(configKey, config.getInt(configKey));
                }

                if (WatchConstants.KEY_AMBIENT_WITH_SCOOTER.equals(configKey)) {
                    needInvalidate = updateUiForKey(configKey, config.getBoolean(configKey));
                }
                if (needInvalidate) {
                    invalidate();
                }
            }
        }

        protected boolean updateUiForKey(String configKey, boolean show) {
            switch (configKey) {
                case WatchConstants.KEY_AMBIENT_WITH_SCOOTER:
                    mConfig_isAmbientWithScooter = show;
                    mBackgroundBitmap = null;
                    break;

                default:
                    return false;
            }
            invalidate();
            return true;
        }

        protected boolean updateUiForKey(String configKey, int value) {
            if (WatchConstants.KEY_HOURS_FORMAT.equals(configKey)) {
                switch (value) {
                    case TimeConstants.HOURS_FORMAT_012:
                        mConfig_isLeadingZero = true;
                        mConfig_is12HourFormat = true;
                        break;
                    case TimeConstants.HOURS_FORMAT_12:
                        mConfig_isLeadingZero = false;
                        mConfig_is12HourFormat = true;
                        break;
                    case TimeConstants.HOURS_FORMAT_024:
                        mConfig_isLeadingZero = true;
                        mConfig_is12HourFormat = false;
                        break;
                    case TimeConstants.HOURS_FORMAT_24:
                        mConfig_isLeadingZero = false;
                        mConfig_is12HourFormat = false;
                        break;
                    default:
                        mConfig_isLeadingZero = true;
                        mConfig_is12HourFormat = true;
                        break;
                }
                setTimeFormat(mConfig_is12HourFormat, mConfig_isLeadingZero);
                setDateFormat(mConfig_isLeadingZero);
                return true;
            }
            return false;
        }

        // ======================= SETTINGS AREA END ================================
        // ======================== HELPERS METHOD AREA ===================
        private void setDateFormat(boolean isLeadingZero) {
            mMonthFormat = new SimpleDateFormat(TimeConstants.DATE_FORMAT_MONTH_CUT, Locale.US);

            mDayFormat = new SimpleDateFormat(
                            ((isLeadingZero) ? TimeConstants.DATE_FORMAT_DAY_WITH_ZERO : TimeConstants.DATE_FORMAT_DAY_WITHOUT_ZERO),
                    Locale.US);
        }

        private void setTimeFormat(boolean is12HourFormat, boolean isLeadingZero) {
            String hoursFormat;
            if (is12HourFormat) {
                hoursFormat = isLeadingZero ? TimeConstants.TIME_FORMAT_012 : TimeConstants.TIME_FORMAT_12;

            } else {
                hoursFormat = isLeadingZero ? TimeConstants.TIME_FORMAT_024: TimeConstants.TIME_FORMAT_24;
            }
            mHourFormat = new SimpleDateFormat(hoursFormat, Locale.US);
            mMinuteFormat= new SimpleDateFormat(TimeConstants.TIME_FORMAT_MIN, Locale.US);
        }
        // ======================== HELPERS METHOD AREA END ===================
    }
}
